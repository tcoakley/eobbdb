<!DOCTYPE html>
<html>
<head>
    <link href='https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">

    <meta name="viewport" content="width=device-width">
    <title>EOBBDB.com</title>

    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" type="text/css" href="css/site.css" />


</head>
<body>
    <div class="pageTitle">EOBBDB.com</div>

    <div class="windowPanel" style="margin: 60px 100px 0 300px; max-width: 600px;">
        <div class="titleBar">Coming Soon</div>
        <div class="topBorder"></div>
        <div class="contentArea">
            <p>
                Soon for a project with one person doing everything, by themselves, in their free time, while still
                working a full time job.   If you are interested in following the progress please check out
                the <a href="http://eobbdb.blogspot.com/">Site Blog</a>.
            </p>
            <p>
                May peace find you.<br>
                Tom Coakley
            </p>
        </div>
    </div>


</body>
</html>
